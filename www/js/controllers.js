angular.module('starter.controllers', [])

.controller('PathologiesCtrl', function($scope, $http) {

  var vm = this;

  getPathologyTypes();
  getPathologies();

  function getPathologyTypes(){
    return $http.get("http://10.0.2.2:8080/api/pathologytypes").then(function(response){
      vm.pathologytypes = response.data;
      return vm.pathologytypes;
    });
  }

  function getPathologies(){
    return $http.get("http://10.0.2.2:8080/api/pathologies").then(function(response){
        vm.pathologies = response.data;
        matchPathologies();
        return vm.pathologies;
      });
  }

  function matchPathologies() {
    vm.pathologytypes.map(
      function callback(currentValue) {
        currentValue.pathologies =
          vm.pathologies.filter(
            function callback(currentValue) {
              return currentValue.pathologytypeId == this.id;
            }, currentValue
          );
        return currentValue;
      });
    }


})

.controller('PathologyCtrl', function($scope, $stateParams, $http) {

  var vm = this;

  getEvidences();


  function getEvidences(){
    return $http.get("http://10.0.2.2:8080/api/evidences", {params : {size : 100000}}).then(function(response){
      vm.evidences =  response.data;
      getPathology();
      return vm.evidences;
    });
  }

  function getPathology(){
    return $http.get("http://10.0.2.2:8080/api/pathologies", {params: {'id.in' : $stateParams.pathologyId}}).then(function(response){
      vm.pathology = response.data;
      getProbiotics();
      return vm.pathology;
    });
  }


    function getProbiotics(){
      return $http.get("http://10.0.2.2:8080/api/probiotics",  {params : {size : 100000}}).then(function(response){
        vm.probiotics = response.data;
        matchEvidences();
        return vm.probiotics;
      });
    }

  function matchEvidences() {
    vm.pathology.map(
      function callback(currentValue) {
        currentValue.evidences =
          vm.evidences.filter(
            function callback(currentValue) {
              return currentValue.pathologyId == this.id;
            }, currentValue
          );
        return currentValue;
      }
    );
    vm.pathology.map(
                function callback(currentValue) {

                    currentValue.probioticInfo =
                        vm.probiotics.filter(
                            function callback(currentValue) {
                                return currentValue.id == this.evidences[0].probioticId;
                            }, currentValue
                        );

                    return currentValue;
                }
            );
    whatAge();
  }

  function whatAge() {
    for (var i = 0; i < vm.pathology.length; i++) {
      vm.ageAdult = false;
      vm.agePedia = false;
      for (var j = 0; j < vm.pathology[i].evidences.length; j++) {
        if (vm.pathology[i].evidences[j].dosageAge == "Adulto") {
          vm.ageAdult = true;
        } else {
          vm.agePedia = true;
        }
      }
      if (vm.ageAdult == true && vm.agePedia == true) {
        vm.pathology[i]['Age'] = 'Ambos';
      }
      else if (vm.ageAdult == true && vm.agePedia == false) {
        vm.pathology[i]['Age'] = 'Adulto';
      }
      else if (vm.ageAdult == false && vm.agePedia == true) {
        vm.pathology[i]['Age'] = 'Pediatrico';
      }
    }
    return vm.pathology;
  }

})

.controller('ReferencesCtrl', function($scope, $stateParams, $http){

var vm = this;
  getReferences();


  function getReferences(){
    return $http.get("http://10.0.2.2:8080/api/evidencereferences", {params: {'id.in' : $stateParams.referenceId}}).then(function(response){
      vm.references =  response.data;
      return vm.references;
    });
  }



})
.controller('AboutCtrl', function($scope){
})
.controller('HomeCtrl', function($scope){
});
